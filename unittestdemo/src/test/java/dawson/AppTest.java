package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void echo_returnsValueGiven_x() {
        assertEquals("Return x", 5, App.echo(5));
    }

    @Test
    public void oneMore_returnsValueGivenPlus_xplus1() {
        assertEquals("Returns x+1", 6, App.oneMore(5));
    }
}
